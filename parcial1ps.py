import sqlite3

conn = sqlite3.connect('parcial1.db')

c = conn.cursor()

# c.execute("""CREATE TABLE inventario2(
   # ID INTEGER PRIMARY KEY AUTOINCREMENT, 
     # ARTICULO VARCHAR(50),
    # DESCRIPCION VARCHAR(200)
 # )""")

conn.commit()

# c.execute("INSERT INTO inventario2 VALUES ('1', 'JABON', 'Sustancia sólida, en polvo o líquida elaborada con la finalidad de limpiar la superficie de algún material sucio.') ")
# c.execute("INSERT INTO inventario2 VALUES ('2', 'LECHE', 'Proviene de la Vaca, está compuesta de grasa, proteínas, lactosa y agua.') ") 
# c.execute("INSERT INTO inventario2 VALUES ('3', 'CEREAL', 'Familia de plantas gramíneas y herbáceas que ostentan granos o semillas que resultan imprescindibles en la base de la alimentación humana y de los animales.') ")
# c.execute("INSERT INTO inventario2 VALUES ('4', 'PAN', 'Producto alimenticio que, por lo general, se elabora con agua, levadura y harina y se cocina en un horno.') ")
# c.execute("INSERT INTO inventario2 VALUES ('5', 'GALLETAS', 'Preparación culinaria de pequeño tamaño, dulce o salada, horneada.') ")

conn.commit()

c.execute("""
    SELECT ID, ARTICULO, DESCRIPCION FROM inventario2
""")

for i in c:
    print ('Articulos:'), i
print ('........................')

conn.commit()

c.execute("""
    UPDATE inventario2 SET ID=2 WHERE ARTICULO='LECHE'
""")

conn.commit()

c.execute("""
    DELETE FROM inventario2 WHERE ID=2
""")

conn.commit()

c.execute("""
    SELECT * FROM inventario2
""")

VariasPalabras = c.fetchall()

print("\n")
print("ARTICULO:" + "\t\t" + "DESCRIPCION:")
print ('.....................')
for inventario2 in VariasPalabras:
    print(inventario2[1], ":\t\t", inventario2[2])

conn.commit()

print ('.....................')
print ('.....................')

conn.commit()

c.execute("""
    SELECT * FROM inventario2 WHERE ARTICULO = 'PAN'
    """)
DESCRIPCION = c.fetchall()
print(DESCRIPCION)

print ('.....................')


conn.commit()





conn.close()
